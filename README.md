# Byens Rens Website

### This project is about the creation of a business website for cleaning. It is concerned with the creation of the website and also setting up a server and configuring a router to make the website public to the Internet.

### For the development of the website it is used Bootstrap 4, HTML, CSS and JavaScript.

### For the hosting of the website a free version of NGINX will be used.

### [Click here](https://anto8460.gitlab.io/byens-rens-website) to see the static website (currently not finished)

### Basic Diagram of the Home Setup:

![](Diagram.PNG)